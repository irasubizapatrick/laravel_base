<?php

namespace App\Http\Controllers;

use App\Card;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    /**
     * @return string
     */
    public function index()
    {
        $user   =Auth::user();
        if($user->hasRole('admin')) {

            $badge = Card::all()->take(5);
            return view('dashboard.admin',compact('badge'));


        }
        else
        {
            return JsonResponse::create(['error' => 'access-denied'],401);
        }

    }
}
