<?php

namespace App\Http\Controllers;

use App\Card;
use Illuminate\Http\Request;

class BadgeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $badge = Card::all();
        return view('pages.badge',compact('badge'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user_id = Auth::id();
        $request->merge(['user_id' => $user_id]);
        $order = Expert::create($request->all());
        $contract_file = $request->file('profile');

        if (isset($contract_file)) {
            if (isset($contract_file)) {
                $extension1 = $request->file('profile')->getClientOriginalExtension();
            }
            $sha = 'profile' . md5(time());
            if (isset($extension1)) {
                $filename1 = date('Y') . $sha . "sgfs." . $extension1;
            }
            $destination_path = 'expert_profile/';
            if (empty($contract_file)) {
                File::makeDirectory($contract_file, 0775, true, true);
                // File::makeDirectory($destination_path, $mode = 0777, true, true);
            }
            if (isset($filename1)) {
                $order->profile = $filename1;
                $request->file('profile')->move($destination_path, $filename1);
                $order->save();
            }
        }
        $all_data = $request->all();
        $all_data['profile'] = !empty($filename1) ? $filename1 : $order->profile;
        $order->save($all_data);
        Session::flash('message', 'Created Successful');
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $contract_doc = $request->file('profile');
        $order = Expert::findOrFail($id);
        if (isset($contract_doc)) {
            if (isset($contract_doc)) {
                $extension1 = $request->file('profile')->getClientOriginalExtension();
            }
            $sha = 'rec_file' . md5(time());
            if (isset($extension1)) {
                $filename1 = date('Y') . $sha . "sgfs." . $extension1;
            }
            $destination_path = 'expert_profile/';
            if (empty($destination_path)) {
                File::makeDirectory($destination_path, 0775, true, true);
                // File::makeDirectory($destination_path, $mode = 0777, true, true);
            }
            if (isset($filename1)) {
                $order->profile = $destination_path . $filename1;
                $request->file('profile')->move($destination_path, $filename1);
                $order->save();
            }

        }
        $data = $request->all();
        $data['profile'] = !empty($filename1) ? $filename1 : $order->profile;
        $order->update($data);
        Session::flash('message', ' Updated Successful');
        return back();
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        $department  =Card::findOrfail($id);
        $department->delete();
        Session::flash('message', ' Deleted Successful');
        return back();
    }
}
