<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Objectif extends Model
{
    use SoftDeletes;
    protected $dates=['deleted_at'];
    protected $table	=   'objectifs';
    protected $fillable	=   ['id', 'user_id','course_id','objective_name'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function course()
    {
        return $this->belongsTo('App\Course', 'course_id');
    }
}
