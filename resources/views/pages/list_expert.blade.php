<!DOCTYPE html>
<html lang="en">

@include('layouts.dash_head')

<body id="page-top">

<div id="wrapper">

    @include('layouts.dash_sidebar')
    <div id="content-wrapper" class="d-flex flex-column">

        <div id="content">
            @include('layouts.dash_nav')
            <div class="container-fluid">


                <h5 class="h5 mb-2 text-gray-800">Expert </h5>
                <div class="card shadow mb-4">
                    <div class="card-header py-3">
                        <button type="button" class="btn btn-primary float-right" data-toggle="modal" data-target="#exampleModal">
                            <span> <i class="fa fa-plus"></i> Add Expert</span>
                        </button>
                        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-lg" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Add Applicant</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <form role="form-horizontal" action="/manage/expert/" method="post" enctype="multipart/form-data">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                        <div class="modal-body">

                                            <div class="col-md-6 float-right">
                                                <label>Profile</label>
                                                <input type="file" name="profile"  class="form-control" required placeholder="Enter full name">
                                            </div>
                                            <div class="col-md-6 ">
                                                <label>Names</label>
                                                <input type="text" name="expert_name"  class="form-control" required placeholder="Enter full name">
                                            </div>
                                            <div class="col-md-6 float-right">
                                                <label>Phone</label>
                                                <input type="number" name="expert_phone"  class="form-control" required placeholder="Enter phone number">
                                            </div>
                                            <div class="col-md-6 ">
                                                <label>Email</label>
                                                <input type="email" name="expert_email"  class="form-control"  placeholder="Enter email address ">
                                            </div>
                                            <div class="col-md-12">
                                                <label>Biography</label>
                                                <textarea name="expert_bio" class="form-control" required>
                                                  </textarea>
                                            </div>

                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-primary">Save changes</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                <thead>
                                <tr >
                                    <th>#</th>
                                    <th>Profile</th>
                                    <th>Names </th>
                                    <th>Phone</th>
                                    <th>Email</th>
                                    <th>Bio-graph</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $i=1;?>
                                @foreach($expert AS $value)
                                    <tr>

                                        <td>{{$i++}}</td>
                                        <td>

                                                <img src="/expert_profile/{{$value->profile}}" alt="" width="100" height="100">


                                        </td>
                                        <td>{{$value->expert_name}}</td>
                                        <td>{{$value->expert_phone}}</td>
                                        <td>{{$value->expert_email}} </td>
                                        <td>{{$value->expert_bio}}</td>

                                        <td>
                                            <button data-toggle="modal" data-target="#edituser<?php echo $i;?>" class="pull-left edit btn btn-success dlt_sm_table"> <span>  <i class="fas fa-check"></i></span></button>
                                            <button type="button" data-toggle="modal" class="tabledit-edit-button btn btn-danger " data-target="#delete<?php echo $i;?> " style="float: none; margin-top: 1rem;"><span class="fas fa-trash"></span></button>
                                            <div class="modal fade" id="delete<?php echo $i;?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLarge01" style="display: none;" aria-hidden="true">
                                                <div class="modal-dialog modal-lg" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title">Delete </h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">×</span>
                                                            </button>
                                                        </div>
                                                        <form   class="form-horizontal mt-1" action="/manage/expert/{{$value->id}}" method="POST">
                                                            <label class="mx-2">Are you sure you want to delete</label>
                                                            <input type="hidden" name="_method" value="DELETE" />
                                                            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                                            <div class="modal-footer">
                                                                <button type="submit" class="btn btn-success">Confirm</button>
                                                                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal fade" id="edituser<?php echo $i;?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLarge01" style="display: none;" aria-hidden="true">
                                                <div class="modal-dialog modal-lg" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title">Edit Expert </h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">×</span>
                                                            </button>
                                                        </div>

                                                        <form role="form-horizontal" action="/manage/expert/{{$value->id}}" method="post" enctype="multipart/form-data">
                                                            <input type="hidden" name="_method" value="PUT" />
                                                            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                                            <div class="modal-body">

                                                                <div class="col-md-6 float-right">
                                                                    <label>Profile</label>
                                                                    <input type="file" name="profile" value="{{$value->profile}}"  class="form-control" required placeholder="Enter full name">
                                                                </div>
                                                                <div class="col-md-6 ">
                                                                    <label>Names</label>
                                                                    <input type="text" name="expert_name" value="{{$value->expert_name}}"  class="form-control" required placeholder="Enter full name">
                                                                </div>
                                                                <div class="col-md-6 float-right">
                                                                    <label>Phone</label>
                                                                    <input type="number" name="expert_phone" value="{{$value->expert_phone}}"  class="form-control" required placeholder="Enter phone number">
                                                                </div>
                                                                <div class="col-md-6 ">
                                                                    <label>Email</label>
                                                                    <input type="email" value="{{$value->expert_email}}" name="expert_email"  class="form-control"  placeholder="Enter email address ">
                                                                </div>
                                                                <div class="col-md-12">
                                                                    <label>Biography</label>
                                                                    <textarea name="expert_bio" class="form-control" required>
                                                                </textarea>
                                                                </div>

                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                                                <button type="submit" class="btn btn-primary">Save changes</button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>

                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>

        </div>
        @include('layouts.dash_footer')
    </div>
</div>

<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>
@include('layouts.dash_js')
</body>

</html>
