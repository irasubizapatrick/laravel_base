
<!DOCTYPE html>
<html lang="en">
@include('layouts.web_head')
<style type="text/css">
    .nav-tabs > li.active > a, .nav-tabs > li.active > a:hover, .nav-tabs > li.active > a:focus{
        color: #fff !important;
        cursor: default !important;
        background-color: #1d74b7 !important;
    }
</style>
<body>
    <div class="wrapper">
        @include('layouts.web_header')
            <section class="course-section__block padding ptb-xs-60">
                <div class="container">
                    <div class="col-md-12">

                        <a href="" role="button" class="btn btn-primary float-right" data-toggle="modal" data-target="#exampleModal"> <i class="fa fa-list"></i> All Courses</a>
                        @foreach($courses_id as $value)
                            <h3 class="mb-20">Course : {{$value->course_name}}</h3>
                        <h4>Course Description :</h4>
                        <p>
                            {{$value->overview}}
                        </p>
                            <a href="{{route ('registration', ['id' =>$value->id])}}" role="button" class="btn btn-warning float-right"> <i class="fa fa-pencil"></i>  Register Here</a>
                        @endforeach

                        <div role="tabpanel">
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="active">
                                    <a href="#home" aria-controls="home" role="tab" data-toggle="tab" aria-expanded="true" style="font-size: larger;font-weight: bold;">Objectives of the course</a>
                                </li>
                                <li role="presentation" class="">
                                    <a href="#profile" aria-controls="profile" role="tab" data-toggle="tab" aria-expanded="false" style="font-size: larger; font-weight: bold;">More Content </a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="home">
                                    <h4>Description</h4>
                                    @foreach($object as $obj)
                                        <ul class="course_features_point">
                                            <li  style="text-align: justify;"><i class="ion-android-done-all text-color"></i> {{$obj->objective_name}} </li>
                                        </ul>
                                    @endforeach
                                </div>
                                <div role="tabpanel" class="tab-pane" id="profile">
                                    <h4>Additional Info</h4>
                                    @foreach($content as $value)
                                        <ul class="course_features_point">
                                            <li  style="text-align: justify;"><i class="ion-android-done-all text-color"></i> {{$value->content_name}} </li>
                                        </ul>
                                    @endforeach
                                </div>
                                <div role="tabpanel" class="tab-pane" id="messages">
                                    <h4>Reviews</h4>
                                    <div class="item">
                                        <p>
                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repudiandae odit iste exercitationem praesentium deleniti nostrum laborum rem id nihil tempora. Adipisci ea commodi unde nam placeat cupiditate quasi a ducimus rem consequuntur ex eligendi minima voluptatem assumenda voluptas quidem sit maiores odio velit voluptate.
                                        </p>
                                        <div class="post-meta">
                                            <!-- Author  -->
                                            <span class="author"> <i class="fa fa-user"></i> John Deo</span>
                                            <!-- Meta Date -->

                                            <span class="time"> <i class="fa fa-calendar"></i> 03.11.2014</span>
                                            <!-- Category -->

                                            <span class="pull-right"></span>
                                            <div class="star-rating">
                                                <span class="pull-right"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-half-o"></i></span>
                                            </div>
                                            <span class="pull-right"></span>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="item">
                                        <p>
                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repudiandae odit iste exercitationem praesentium deleniti nostrum laborum rem id nihil tempora. Adipisci ea commodi unde nam placeat cupiditate quasi a ducimus rem consequuntur ex eligendi minima voluptatem assumenda voluptas quidem sit maiores odio velit voluptate.
                                        </p>
                                        <div class="post-meta">
                                            <!-- Author  -->
                                            <span class="author"> <i class="fa fa-user"></i> John Deo</span>
                                            <!-- Meta Date -->

                                            <span class="time"> <i class="fa fa-calendar"></i> 03.11.2014</span>
                                            <!-- Category -->

                                            <span class="pull-right"></span>
                                            <div class="star-rating">
                                                <span class="pull-right"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-half-o"></i></span>
                                            </div>
                                            <span class="pull-right"></span>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="item">
                                        <p>
                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repudiandae odit iste exercitationem praesentium deleniti nostrum laborum rem id nihil tempora. Adipisci ea commodi unde nam placeat cupiditate quasi a ducimus rem consequuntur ex eligendi minima voluptatem assumenda voluptas quidem sit maiores odio velit voluptate.
                                        </p>
                                        <div class="post-meta">
                                            <!-- Author  -->
                                            <span class="author"> <i class="fa fa-user"></i> John Deo</span>
                                            <!-- Meta Date -->

                                            <span class="time"> <i class="fa fa-calendar"></i> 03.11.2014</span>
                                            <!-- Category -->

                                            <span class="pull-right"></span>
                                            <div class="star-rating">
                                                <span class="pull-right"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-half-o"></i></span>
                                            </div>
                                            <span class="pull-right"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        @include('layouts.web_footer')
    </div>
    @include('layouts.web_js')
</body>
</html>