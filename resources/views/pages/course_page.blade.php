
<!DOCTYPE html>
<html lang="en">
@include('layouts.web_head')
<body>
<div class="wrapper">
@include('layouts.web_header')
<!--Header End Here-->
    <!-- Intro Section -->
    <section class="course-section__block padding ptb-xs-60">
        <div class="container">
            <div class="row">
                @foreach($courses as $value)
                    <div class="col-sm-9 mb-30">
                        <div class="course__details_block">
                            <div class="course__content_block mt-30">
                                <h2 class="mb-20">Course : {{$value->course_name}}</h2>
                                <p>{{$value->overview}}</p>
                                <a href="{{route ('view/course/more', ['id' =>$value->id])}}" class="more_btn__block"  style="color:#feb20e;">  View more details <i class="fa fa-angle-right"></i></a>
                            </div>
                        </div>
                    </div>

                @endforeach

            </div>

        </div>
    </section>
    @include('layouts.web_footer')
</div>
@include('layouts.web_js')
</body>
</html>