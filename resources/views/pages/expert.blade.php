
<!DOCTYPE html>
<html lang="en">
@include('layouts.web_head')
<body>
<div class="wrapper">
@include('layouts.web_header')
<!-- End Intro Section -->
    <div class=" padding ptb-xs-60">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <!--Team Section-->
                    <section id="team" class="team-section">

                        <div class="container">
                            <div class="row text-center pb-30">
                                <div class="col-sm-12">
                                    <div class="heading-box ">
                                        <h2><span>Our</span> Trainers</h2>
                                        <span class="b-line"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="  team-pagination">
                                        {{--<div class="team-member col-md-4 col-sm-6 col-xs-12">--}}
                                            {{----}}
                                            {{--<div class="inner-box wow fadeInUp animated">--}}
                                                {{--<div class="image-box">--}}
                                                    {{--<img src="/assets/images/team/team4.jpg" alt="">--}}
                                                    {{--<!--Overlay Box-->--}}
                                                    {{--<div class="overlay-box">--}}
                                                        {{--<!--User Info-->--}}
                                                        {{--<div class="user-info">--}}
                                                            {{--<div class="text">--}}
                                                                {{--Senior Lecturer--}}
                                                            {{--</div>--}}
                                                            {{--<h4>Elizabeth Jones</h4>--}}
                                                        {{--</div>--}}
                                                        {{--<!--Social Icon Two-->--}}
                                                        {{--<ul class="social-icon-two">--}}
                                                            {{--<li>--}}
                                                                {{--<a href="#"><span class="fa fa-facebook"></span></a>--}}
                                                            {{--</li>--}}
                                                            {{--<li>--}}
                                                                {{--<a href="#"><span class="fa fa-twitter"></span></a>--}}
                                                            {{--</li>--}}
                                                            {{--<li>--}}
                                                                {{--<a href="#"><span class="fa fa-google-plus"></span></a>--}}
                                                            {{--</li>--}}
                                                        {{--</ul>--}}
                                                    {{--</div>--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}

                                    </div>
                                </div>
                            </div>

                            <div class="row mt-80">
                                <div class="col-sm-12">
                                    <div class="team-pagination" >
                                        <!--Team Member-->
                                        {{--protected $fillable	=   ['id', 'user_id','expert_name','expert_phone','expert_email','expert_bio','profile'];--}}
                                        @foreach($expert as $value)
                                        <div class="team-member col-md-3 col-sm-6 col-xs-12">
                                            <div class="inner-box">
                                                <div class="image-box">
                                                    <img src="/expert_profile/{{$value->profile}}" alt="" >

                                                    <!--Overlay Box-->
                                                    <div class="overlay-box">
                                                        <!--User Info-->
                                                        <div class="user-info">
                                                            <h4>{{$value->expert_name}}</h4>
                                                            <p>{{$value->expert_phone}}</p>

                                                        </div>
                                                        <!--Social Icon Two-->
                                                        <ul class="social-icon-two">
                                                            <li>
                                                                <a href="#"><span class="fa fa-facebook"></span></a>
                                                            </li>
                                                            <li>
                                                                <a href="#"><span class="fa fa-twitter"></span></a>
                                                            </li>
                                                            <li>
                                                                <p>{{$value->expert_email}}</p>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
    @include('layouts.web_footer')
</div>

@include('layouts.web_js')
</body>
</html>
