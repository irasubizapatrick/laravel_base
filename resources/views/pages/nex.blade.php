
<!DOCTYPE html>
<html lang="en">
@include('layouts.web_head')
<body>
<div class="wrapper">
@include('layouts.web_header')
<!--Header End Here-->
    <!-- Intro Section -->
    <section class="course-section__block padding ptb-xs-60">
        <div class="container">
            <div class="row">
                @foreach($category as $value)
                    <div class="col-sm-9 mb-30">
                        <div class="course__details_block">
                            <div class="course__text_details mt-40">
                                <h3 class="mb-20">{{$value->category_name}}</h3>
                                <h2>Description</h2>
                                <p>{{$value->category}}</p>
                            </div>
                            <div class="course__content_block mt-30">
                                <h2 class="mb-20">Course : {{$value->course_name}}</h2>
                                <p>{{$value->overview}}</p>
                                <h2 class="mt-20">Objective</h2>
                                {{--<div class="row">--}}
                                {{--<div class="col-md-12">--}}
                                {{--@foreach($object as $obj)--}}
                                {{--<ul class="course_features_point">--}}
                                {{--<li><i class="fa fa-star"></i> {{$obj->objective_name}} </li>--}}
                                {{--</ul>--}}
                                {{--@endforeach--}}
                                {{--</div>--}}
                                {{--</div>--}}

                            </div>
                            <div class="course__content_block mt-30">
                                <h2 class="mb-20">Content</h2>
                                <ul class="course_features_point">
                                    <li><i class="fa fa-star"></i> Start: 15 May, 2018 </li>
                                </ul>

                            </div>

                        </div>

                    </div>
                @endforeach
                <div class="col-sm-3 mt-sm-60">
                    <div class="sidebar-widget">
                        <h4>Search</h4>
                        <div class="widget-search pt-15">
                            <input class="form-full input-lg" type="text" value="" placeholder="Search Here" name="search" id="wid-search">
                            <input type="submit" value="" name="email" id="wid-s-sub">
                        </div>
                    </div>
                    <div class="sidebar-widget">
                        <h4>ALL COURSES</h4>

                        <ul class="categories">
                            <li><a href="courses-details.html#"><i class="fa fa-chevron-right"></i> languages</a></li>
                            <li><a href="courses-details.html#"><i class="fa fa-chevron-right"></i> digital literacy</a></li>
                            <li><a href="courses-details.html#"><i class="fa fa-chevron-right"></i> business</a></li>
                            <li><a href="courses-details.html#"><i class="fa fa-chevron-right"></i> it skills</a></li>
                            <li><a href="courses-details.html#"><i class="fa fa-chevron-right"></i> health literacy</a></li>
                            <li><a href="courses-details.html#"><i class="fa fa-chevron-right"></i> photography</a></li>
                            <li><a href="courses-details.html#"><i class="fa fa-chevron-right"></i> spoken</a></li>
                        </ul>
                    </div>
                    <div class="sidebar-widget">
                        <h4>Course Type</h4>

                        <ul class="categories">
                            <li><a href="courses-details.html#"><i class="fa fa-chevron-right"></i> all</a></li>
                            <li><a href="courses-details.html#"><i class="fa fa-chevron-right"></i> paid</a></li>
                            <li><a href="courses-details.html#"><i class="fa fa-chevron-right"></i> free</a></li>
                        </ul>
                    </div>
                </div>
            </div>

        </div>
    </section>
    @include('layouts.web_footer')
</div>
@include('layouts.web_js')
</body>
</html>