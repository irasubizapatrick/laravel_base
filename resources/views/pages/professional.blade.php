
<!DOCTYPE html>
<html lang="en">
@include('layouts.web_head')
<body>
<div class="wrapper">
    @include('layouts.web_header')
    <section class="course-section__block padding ptb-xs-60">
        <div class="container">
            <div class="row">
                <h3 style="padding-left: 1%;color: #feb20d;font-weight: 600;">Professional Training  Courses List</h3>
                {{--@foreach($courses as $value)--}}
                {{--<div class="col-sm-9 mb-30">--}}
                {{--<div class="course__details_block">--}}
                {{--<div class="course__content_block mt-30">--}}
                {{--<h2 class="mb-20">Course : {{$value->course_name}}</h2>--}}
                {{--<p>{{$value->overview}}</p>--}}
                {{--<a href="{{route ('view/course/more', ['id' =>$value->id])}}" class="more_btn__block"  style="color:#feb20e;">  View more details <i class="fa fa-angle-right"></i></a>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</div>--}}
                <div class="col-sm-12">
                    <div class="cart_wrpaer">
                        <div class="table_scroll table-responsive">
                            <table class=" table table-striped">
                                <thead class="dark-bg">
                                <tr>
                                    <th><span style="font-size: 17px;">Course Code</span></th>
                                    <th><span style="font-size: 17px;">Course Title</span></th>
                                    <th><span style="font-size: 17px">Register</span></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $i=1;?>
                                @forelse($professional AS $value)
                                    <tr>
                                        <td style="font-size: 15px;"> {{$i++}} </td>
                                        <td style=" font-size: 17px;">
                                            <a href="{{route ('view/course/more', ['id' =>$value->id])}}"  style="color:black !important;">  {{$value->course_name}}  <i class="fa fa-angle-right " style="color: #337ab7;font-weight: 600;">  read more</i>  </a>

                                        </td>
                                        <td style="font-size: 17px;">
                                            <a href="{{route ('registration', ['id' =>$value->id])}}"  class="btn btn-primary"  style="color:#ffffff !important;">  Register </a>

                                        </td>
                                    </tr>
                                    @empty

                                    <tr>
                                        <td style=" font-size: 17px;">
                                        </td>
                                        <td>
                                            <span style="color: red; font-size: larger;"> no content</span>
                                        </td>

                                        <td></td>
                                    </tr>
                                @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>
    @include('layouts.web_footer')
</div>
@include('layouts.web_js')
</body>
</html>