
<!DOCTYPE html>
<html lang="en">
@include('layouts.web_head')
<body>
<div class="wrapper">
  @include('layouts.web_header')
    <section class="course-section__block padding ptb-xs-60">
        <div class="container">
            <div class="row">
                @foreach($category as $value)
                <div class="col-sm-9 mb-30">
                    <div class="course__details_block">
                        <div class="course__text_details mt-40">
                            <h2 class="mb-20">{{$value->category_name}}</h2>
                            <h3>Descriptions</h3>
                            <p>{{$value->category}}</p>
                        </div>
                    </div>
                    <a href="{{route ('courses/list', ['id' =>$value->id])}}" class="more_btn__block"  style="color:#feb20e;">  View Courses <i class="fa fa-angle-right"></i></a>
                </div>
                @endforeach
        </div>
    </section>
    @include('layouts.web_footer')
</div>
@include('layouts.web_js')
</body>
</html>