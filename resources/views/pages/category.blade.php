<!DOCTYPE html>
<html lang="en">

@include('layouts.dash_head')

<body id="page-top">

<div id="wrapper">

@include('layouts.dash_sidebar')
    <div id="content-wrapper" class="d-flex flex-column">

        <div id="content">
          @include('layouts.dash_nav')
            <div class="container-fluid">


                <h5 class="h5 mb-2 text-gray-800">Course Category</h5>
                <div class="card shadow mb-4">
                    <div class="card-header py-3">
                        <!-- Button trigger modal -->
                        <button type="button" class="btn btn-primary float-right" data-toggle="modal" data-target="#exampleModal">
                            <span> <i class="fa fa-plus"></i> Add category</span>
                        </button>

                        <!-- Modal -->
                        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-lg" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Add Category</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <form role="form-horizontal" action="/category" method="post" enctype="multipart/form-data">
                                        <div class="modal-body">
                                                <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                                <div class="modal-body">
                                                    <div class="col-md-12">
                                                        <label> Category</label>
                                                        <input type="text" class="form-control" name="category_name"  placeholder="Enter category Name" required/>

                                                    </div>
                                                    <div class="col-md-12">
                                                        <label>Description</label>
                                                        <textarea type="text" class="form-control" name="category"   required placeholder="Enter description">
                                                            </textarea>
                                                    </div>
                                                </div>

                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-primary">Save changes</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                <thead>
                                <tr >
                                    <th>#</th>
                                    <th>CategoryName </th>
                                    <th>Description</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $i=1;?>
                                @foreach($category AS $value)
                                    <tr>
                                        <td>{{$i++}}</td>
                                        <td>{{$value->category_name}}</td>
                                        <td>{{$value->category}} </td>
                                        <td>
                                            <button data-toggle="modal" data-target="#edituser<?php echo $i;?>" class="pull-left edit btn btn-success dlt_sm_table"> <span>  <i class="fas fa-check"></i></span></button>
                                            <button type="button" data-toggle="modal" class="tabledit-edit-button btn btn-danger mt-2" data-target="#delete<?php echo $i;?>" style="float: none;"><span class="fas fa-trash"></span></button>

                                            <div class="modal fade" id="delete<?php echo $i;?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLarge01" style="display: none;" aria-hidden="true">
                                                <div class="modal-dialog modal-lg" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title">Delete </h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">×</span>
                                                            </button>
                                                        </div>
                                                        <form   class="form-horizontal mt-1" action="/category/{{$value->id}}" method="POST">
                                                            <label class="mx-2">Are you sure you want to delete</label>
                                                            <input type="hidden" name="_method" value="DELETE" />
                                                            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                                            <div class="modal-footer">
                                                                <button type="submit" class="btn btn-success">Confirm</button>
                                                                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal fade" id="edituser<?php echo $i;?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLarge01" style="display: none;" aria-hidden="true">
                                                <div class="modal-dialog modal-lg" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title">Edit Category </h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">×</span>
                                                            </button>
                                                        </div>

                                                        <form role="form-horizontal" action="/category/{{$value->id}}" method="post" enctype="multipart/form-data">
                                                            <input type="hidden" name="_method" value="PUT" />
                                                            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                                            <div class="modal-body">
                                                                <div class="col-md-12">
                                                                    <label> Category</label>
                                                                    <input type="text" class="form-control"  value="{{$value->category_name}}" name="category_name"   required>

                                                                </div>
                                                                <div class="col-md-12">
                                                                    <label>Description</label>
                                                                    <textarea type="text" class="form-control" name="category"   required>
                                                                           {{$value->category}}
                                                                        </textarea>
                                                                </div>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                                                <button type="submit" class="btn btn-primary">Save changes</button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>

                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>

        </div>
        @include('layouts.dash_footer')

    </div>

</div>

<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>

<!-- Logout Modal-->
<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                <a class="btn btn-primary" href="login.html">Logout</a>
            </div>
        </div>
    </div>
</div>

@include('layouts.dash_js')
</body>

</html>
