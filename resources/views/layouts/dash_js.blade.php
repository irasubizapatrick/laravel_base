<script src="/dash/vendor/jquery/jquery.min.js"></script>
<script src="/dash/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="/dash/vendor/jquery-easing/jquery.easing.min.js"></script>
<script src="/dash/js/sb-admin-2.min.js"></script>
<script src="/dash/vendor/chart.js/Chart.min.js"></script>
<script src="/dash/js/demo/chart-area-demo.js"></script>
<script src="/dash/js/demo/chart-pie-demo.js"></script>
<script src="/dash/vendor/datatables/jquery.dataTables.min.js"></script>
<script src="/dash/vendor/datatables/dataTables.bootstrap4.min.js"></script>
<script src="/dash/js/demo/datatables-demo.js"></script>
<script src="/dash/js/initial.js-master/initial.js" type="text/javascript"></script>

<script>
    $('.profile').initial();
</script>