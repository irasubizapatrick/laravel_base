<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>ON Training Academy</title>
    <link rel="shortcut icon" type="image/x-icon" href="favicon.ico">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600,700,800%7CLato:300,400,700" rel="stylesheet" type="text/css">
    <link href="/assets/css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="/assets/css/font-awesome.css" rel="stylesheet" type="text/css">
    <link href="/assets/css/ionicons.css" rel="stylesheet" type="text/css">
    <link href="/assets/css/jquery.fancybox.css" rel="stylesheet" type="text/css">
    <link href="/assets/css/prettyPhoto.css" rel="stylesheet" />
    <link href="/assets/css/plugin/owl.carousel.css" rel="stylesheet" type="text/css">
    <link href="/assets/css/settings.css" type="text/css" rel="stylesheet" media="screen">
    <link href="/assets/css/layers.css" type="text/css" rel="stylesheet" media="screen">
    <link href="/assets/css/style.css" rel="stylesheet">
    <link href="/assets/css/bootsnav2.css" rel="stylesheet">
    <link href="/assets/css/index3.css" rel="stylesheet">
    <link type="text/javascript" href="/assets/js/jquery-1.12.4.min.js">
</head>