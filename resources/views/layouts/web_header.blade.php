<header>
    <div class="top-part__block ptb-15">
        <div class="container">
            <div class="row">
                <div class="col-sm-7">
                    <p style="color: white;">Welcome to  ON Training Academy </p>
                </div>
                <div class="col-sm-5">
                    <div class="social-link__block text-right">
                        <a href="#"><i class="fa fa-facebook"></i></a>
                        <a href="#"><i class="fa fa-twitter"></i></a>
                        <a href="#"><i class="fa fa-google-plus"></i></a>
                        <a href="#"><i class="fa fa-linkedin"></i></a>
                        <a href="#"><i class="fa fa-search"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="middel-part__block">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-lg-4">
                    <div class="logo">
                        <a href="/">
                            <img src="/assets/images/on.png" alt="Logo">
                        </a>
                    </div>
                </div>
                <div class="col-md-9 col-lg-8">
                    <div class="top-info__block text-right">
                        <ul>
                            <li>
                                <i class="fa fa-map-marker"></i>
                                <p>4 KG 11 Ave, Kigali, Remera <span>Po.BOX 4510, YYUSSA PLAZA	</span></p>
                            </li>
                            <li>
                                <i class="fa fa-envelope-o"></i>
                                <p>Email <span>info@ontraininacademy.rw</span></p>
                            </li>
                            <li>
                                <i class="fa fa-phone"></i>
                                <p>(+250) 783 075 259 <span>(+250) 788 273 855</span></p>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="main_nav stricky-header__top">

        <nav class="navbar navbar-default navbar-sticky bootsnav">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu"> <i class="fa fa-bars" style="width: 70px;"></i> </button>
                </div>
                <div class="collapse navbar-collapse" id="navbar-menu">

                    <ul class="nav navbar-nav mobile-menu">
                        <li>
                            <a href="/">Home</a>
                            <span class="submenu-button"></span>
                        </li>
                        <li>
                            <a href="/about">About us</a>
                            <span class="submenu-button"></span>
                        </li>
                        <li> <a href="javascript:avoid(0);">Course</a> <span class="submenu-button"></span>
                            <ul class="dropdown-menu">
                                <li> <a href="/courses/executive">Executive Training </a> </li>
                                <li> <a href="/courses/professional">Professional Training </a> </li>
                            </ul>
                        </li>

                        <li> <a href="/trainers/list">Trainers</a> <span class="submenu-button"></span>

                        </li>
                        <li> <a href="javascript:avoid(0);">Jobs</a> <span class="submenu-button"></span>
                            <ul class="dropdown-menu">
                                <li> <a href="#">All Jobs</a> </li>

                            </ul>
                        </li>

                        <li> <a href="#">Publications</a> <span class="submenu-button"></span>

                        </li>
                        <li>
                            <a href="/contact">Contact us</a> <span class="submenu-button"></span>

                        </li>
                        <li><a class="custom-btn" href="#">Subscribe Now</a></li>
                    </ul>
                </div>
            </div>
        </nav>
    </div>
</header>