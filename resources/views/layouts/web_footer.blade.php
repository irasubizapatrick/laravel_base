<footer>
    <div class="main_footer__block pb-0 pt-60">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <div class="footer_box__block">
                        <h4 class="footer_menu">About Us</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Autem, architecto, asperiores. Recusandae ea a culpa eligendi, harum amet cumque quod.</p>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid sequi, fuga rem aperiam expedita ipsum.</p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="footer_box__block">
                        <h4 class="footer_menu">Latest Blog Post</h4>
                        <ul>
                            <li><a href="#">Start your own agency</a></li>
                            <li><a href="#">How to cool down quality</a></li>
                            <li><a href="#">Make something awesome</a></li>
                            <li><a href="#">Plane your summer vacation</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="footer_box__block">
                        <h4 class="footer_menu">Our Project</h4>
                        <ul>
                            <li><a href="#">Go get an ice-cream</a></li>
                            <li><a href="#">Become the best version</a></li>
                            <li><a href="#">Eat, Sleep and have fun</a></li>
                            <li><a href="#">Start the journy to the top</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="footer_box__block address-box">
                        <h4 class="footer_menu">Contact info</h4>
                        <ul>
                            <li> <i class="fa fa-phone"></i>
                                <p> (+250) 783 075 259 </p>
                            </li>
                            <li> <i class="fa fa-envelope-o"></i>
                                <p><a href="mailto:info@ontrainingacademy.rw">info@ontrainingacademy.rw</a></p>
                            </li>
                            <li> <i class="fa fa-map-marker"></i>
                                <p>4 KG 11 Ave, Kigali, Remera</p>
                            </li>
                            <li> <i class="fa fa-clock-o"></i>
                                <p> We work 24/7</p>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="copyriight_block ptb-20 mt-20 ">
                <div class="row">
                    <div class="col-md-12 mx-auto text-center copyright_footer ">
                        <p>All Rights Reserved 	&copy; 2019</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>